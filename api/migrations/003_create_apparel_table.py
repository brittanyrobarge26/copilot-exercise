steps = [
    [
        # "Up" SQL statement
        """
        CREATE TABLE apparels (
            id SERIAL PRIMARY KEY NOT NULL,
            item VARCHAR(100) NOT NULL,
            description VARCHAR(256) NOT NULL,
            size VARCHAR(100) NOT NULL,
            price INTEGER NOT NULL
        );
        """,
        # "Down" SQL statement
        """
        DROP TABLE apparels;
        """
    ],
]
