from fastapi import APIRouter, HTTPException
from queries.shop import create_shop, read_shops, read_shop_by_id, update_shop, delete_shop, ShopOut, ShopIn


router = APIRouter()

@router.post("/shops", response_model=ShopOut)
async def create_shop_endpoint(shop: ShopIn):
    new_shop = create_shop(shop)
    if new_shop is None:
        raise HTTPException(status_code=400, detail="Shop could not be created")
    return new_shop

@router.get("/shops", response_model=list[ShopOut])
async def read_shops_endpoint():
    shops = read_shops()
    return shops

@router.get("/shops/{shop_id}", response_model=ShopOut)
async def read_shop_by_id_endpoint(shop_id: int):
    shop = read_shop_by_id(shop_id)
    if shop is None:
        raise HTTPException(status_code=404, detail="Shop not found")
    return shop

@router.patch("/shops/{shop_id}", response_model=ShopOut)
async def update_shop_endpoint(shop_id: int, shop: ShopIn):
    updated_shop = update_shop(shop_id, shop)
    if updated_shop is None:
        raise HTTPException(status_code=404, detail="Shop not found")
    return updated_shop

@router.delete("/shops/{shop_id}")
async def delete_shop_endpoint(shop_id: int):
    deleted = delete_shop(shop_id)
    if not deleted:
        raise HTTPException(status_code=404, detail="Shop not found")
    return {"detail": "Shop deleted"}
